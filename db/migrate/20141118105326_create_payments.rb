class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :bank_ifsc_code
      t.string :bank_account_number
      t.string :amount
      t.string :merchant_transaction_ref
      t.date   :merchant_transaction_date
      t.string :payment_gateway_merchant_reference
      t.timestamps
    end
  end
end
