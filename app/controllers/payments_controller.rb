class PaymentsController < ApplicationController
  def create 
    payload_to_pg = BASE64.decode64(params[:msg])
    payload_params = payload_to_pg.split("|")
    data = []  
    payment= Payment.new
    payload_params.each do |payload|
      d = payload.split("=")
      payment.send(d[0]) = d[1]
    end
    if payment.save
   	render json: {txn_status: "success", amount: payment.amount, merchant_transaction_ref: payment.merchant_transaction_ref, merchant_transaction_date:  payment.merchant_transaction_date, payment_gateway_merchant_reference: payment.payment_gateway_merchant_reference , payment_gateway_transaction_reference: payment.payment_gateway_transaction_reference}
    else
   	render json: {txn_status: "failure", amount: payment.amount, merchant_transaction_ref: payment.merchant_transaction_ref, merchant_transaction_date:  payment.merchant_transaction_date, payment_gateway_merchant_reference: payment.payment_gateway_merchant_reference , payment_gateway_transaction_reference: payment.payment_gateway_transaction_reference}
     end
  end
end
