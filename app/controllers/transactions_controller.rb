class TransactionsController < ApplicationController

  def new
    @transaction = Transaction.new
  end

  def create
    Transaction.transaction do
      transaction = Transaction.new(transaction_params)
      transaction.pay_and_save
    end
  end

  private

  def transaction_params
    params.require(:transaction).permit(:bank_ifsc_code, :bank_account_number, :amount, :merchant_transaction_ref, :merchant_transaction_date, :payment_gateway_merchant_reference)
  end
end
