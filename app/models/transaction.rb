class Transaction < ActiveRecord::Base
   require 'digest/sha1'
   require 'net/http'

   attr_accessor :bank_ifsc_code, :bank_account_number, :amount, :merchant_transaction_ref, :merchant_transaction_date, :payment_gateway_merchant_reference

   def pay_and_save
     payload_without_sha = ["bank_ifsc_code=#{self.bank_ifsc_code}", "bank_account_number=#{self.bank_account_number}", "amount=#{self.amount}", 
                            "merchant_ransaction_ref=#{self.merchant_transaction_ref}", "merchant_transaction_date=#{self.merchant_transaction_date}",
                            "payment_gateway_merchant_reference=#{self.payment_gateway_merchant_reference}"
                           ].join("|")
     payload_with_sha = payload_without_sha + "|hash=#{generate_sha(payload_without_sha)}" 
     payload_to_pg = Base64.encode64(payload_with_sha)
     send_request(payload_to_pg)
   end

   def generate_sha(str)
     Digest::SHA1.hexdigest str
   end

   def send_request(msg)
     uri = URI('http://localhost:3000/payments')
     res = Net::HTTP.post_form(uri, :msg => msg)
   end
end
